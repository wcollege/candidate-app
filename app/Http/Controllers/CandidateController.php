<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidate;
use Illuminate\Support\Facades\Auth;
use App\Role\RoleChecker;

class CandidateController extends Controller
{
    //

    public function findAll() {


        $candidates = Candidate::All();
        return response()->json([
            'meta' => [
                'success' => true,
                "erros"   => []
            ],
            'data' => $candidates
        ]);

    }

    public function findMyCandidates() {

        

        $user = Auth::guard()->user();

        $owner = $user->id;
        $candidates = Candidate::where(['owner' => $owner])->get();
        return response()->json([
            'meta' => [
                'success' => true,
                "erros"   => []
            ],
            'data' => $candidates
        ]);
    }

    public function store(Request $request) {

        

        $candidate = new Candidate;
        $candidate->name   = $request->input('name');
        $candidate->source = $request->input('source');
        $candidate->owner  = $request->input('owner');
        $candidate->created_by  = $request->input('created_by');

        $res = $candidate->save();

        

        if ($res) {
            return back()->with('status', 'Candidate has been created sucessfully');
        }

        return back()->withErrors(['msg', 'There was an error saving the comment, please try again later']);


        
    }
}
