<?php 
namespace App\Role;

use App\Models\User;

class RoleChecker
{
    /**
     * Check if the user has the specified role.
     *
     * @param  \App\Models\User  $user
     * @param  string  $role
     * @return Boolean
     */
    public function check(User $user, string $role) {
        // Manager has everything
        if($user->hasRole(UserRole::ROLE_MANAGER)){
            return true;
        } else if($user->hasRole(UserRole::ROLE_AGENT)) {
            
            $managementRoles = UserRole::getAllowedRoles(UserRole::ROLE_AGENT);
            if(in_array($role, $managementRoles)) {
                return true;
            }
        }

        return $user->hasRole($role);
    }
}