<?php

namespace App\Role;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRole {
    
    const ROLE_MANAGER = 'Manager';
    const ROLE_AGENT = 'Agent';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected static $roleHierarchy = [
        self::ROLE_MANAGER => ['*'],
        self::ROLE_AGENT => [''],
    ];

    /**
     * The table associated with the model.
     * 
     * @param string $role
     * @return array
     */
    public static function getAllowedRoles(string $role) {
        echo self::$roleHierarchy[$role]; die;
        if(isset(self::$roleHierarchy[$role])){
            return self::$roleHierarchy[$role];
        }

        return [];
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @return  array
     */

     public static function getRoleList(){
        return [
                    self::ROLE_MANAGER => 'Manager',
                    self::ROLE_AGENT   => 'Agent',
                ];
     }
}
