<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Role\UserRole;
use App\Models\Role;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CandidateController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('user', function () {
    echo Hash::make('1234'); die;
    //
    /*$roles =  Role::inRandomOrder()->first();
    dd($roles);*/
});//->middleware('check_user_role:'.UserRole::ROLE_MANAGER);


Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');

});

Route::controller(CandidateController::class)->group(function () {
    //Route::post('candidates', 'store');
    Route::get('candidates/lead', 'findAll')->middleware('check_user_role:'.UserRole::ROLE_MANAGER);
    Route::get('candidates/mylead', 'findMyCandidates')->middleware('check_user_role:'.UserRole::ROLE_AGENT);

});
