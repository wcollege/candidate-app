<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Candidate;

class CandidateTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {

        $user = User::factory()->create();
        // Creamos el candidato
        $test_candidate = [
            'name' => 'Nuevo candidato',
            'source' => 'Fotocasa',
            'owner' => $user->id,
            'created_by' => $user->id
        ];

        /*$response = $this->get('/');

        $response->assertStatus(200);*/

        $response = $this->post('/candidates', $test_candidate);
        //$response = $this->get('/',$test_candidate);
        $response->assertSessionHas('status', 'Candidate has been created sucessfully');
    }
}
